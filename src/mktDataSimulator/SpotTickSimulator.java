package mktDataSimulator;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class SpotTickSimulator implements Runnable {

	public void run () {
		ScheduledExecutorService ses = Executors.newScheduledThreadPool(2);

		HashMap <String, String> newSpots = new HashMap <String, String>();
		List<String> TickerList = RedisCacheOperations.readSetFromCache("TickerList");
		HashMap <String, GeometricBrownian> simulations = new HashMap <String, GeometricBrownian>();

		System.out.println("Initialising Tick Simulation");
		int seed = 0;		
		for (String s : TickerList){
			String initialSpot = RedisCacheOperations.readHashFromCache("TickerInfo", s+"_Price");
			String tickerVol = RedisCacheOperations.readHashFromCache("TickerInfo", s+"_TickVol");
			GeometricBrownian process = new GeometricBrownian(0, Double.valueOf(tickerVol), Double.valueOf(initialSpot) , 1, 1, ++seed);
			simulations.put(s, process);
		}

		Runnable saveSpots = () -> {
			//System.out.println("Saving new Ticks to db ...");
			RedisCacheOperations.writeHashToCache("TickerInfo", newSpots);
			//System.out.println("Saved new Ticks to db ...");
		};

		Runnable updateSpots = () -> {
			//System.out.println("Calcing new spots ...");
			for (String s : TickerList){
				String newSpot = simulations.get(s).getSequence();
				newSpots.put(s+"_Spot", newSpot);
			}
			Thread dbSave = new Thread(saveSpots);
			dbSave.start();
			//System.out.println("Done calcing new spots ...");
		};

		System.out.println("Starting Tick Simulation");
		ScheduledFuture<?> scheduledFuture = ses.scheduleAtFixedRate(updateSpots, 1, 1, TimeUnit.SECONDS);
		RedisCacheOperations.writeStringToCache("SimulateSpots", "ON");

		while (true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
			if (RedisCacheOperations.readStringFromCache("SimulateSpots") == null ||
					RedisCacheOperations.readStringFromCache("SimulateSpots").equalsIgnoreCase("ON") == false) {
				System.out.println("Stopping Tick Simulation");
				scheduledFuture.cancel(true);
				ses.shutdown();
				break;
			}
		}
	}
}
