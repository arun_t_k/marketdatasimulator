package mktDataSimulator;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class UserGuiManager {
	UserGuiManager(){
		JFrame guiFrame = new JFrame();		
		guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		guiFrame.setTitle("Simulator Control");
		guiFrame.setSize(400,200);
		guiFrame.setLocationRelativeTo(null);
		
		final JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(0,1));
		JButton btnSetEnv = new JButton("Setup DB");
		JButton btnLoadTickers = new JButton("Load Tickers");
		JButton btnLoadOrdersWeb = new JButton("Load Orders from Web");
		JButton btnLoadOrdersFile = new JButton("Load Orders from File");
		JButton btnSpotTick = new JButton("Toggle Spot Simulation");
		JButton btnClearCache = new JButton("Clear Cache");
		buttonPanel.add(btnSetEnv);
		buttonPanel.add(btnLoadTickers);
		buttonPanel.add(btnLoadOrdersWeb);
		buttonPanel.add(btnLoadOrdersFile);
		buttonPanel.add(btnSpotTick);
		buttonPanel.add(btnClearCache);
		
		final JPanel labelPanel = new JPanel();
		labelPanel.setLayout(new GridLayout(0,1));
		JLabel lblSetEnv = new JLabel("No DB set");
		JLabel lblLoadTickers = new JLabel("No Tickers");
		JLabel lblLoadOrdersWeb = new JLabel("No Orders from Web");
		JLabel lblLoadOrdersFile = new JLabel("No Orders from File");
		JLabel lblSpotTick = new JLabel("Spot Simulation Off");
		JLabel lblClearCache = new JLabel("-");
		labelPanel.add(lblSetEnv);
		labelPanel.add(lblLoadTickers);
		labelPanel.add(lblLoadOrdersWeb);
		labelPanel.add(lblLoadOrdersFile);
		labelPanel.add(lblSpotTick);
		labelPanel.add(lblClearCache);
		
		btnSetEnv.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event) {
				SimulatorCore.setCache(new RedisCacheManager());
				lblSetEnv.setText("Redis cache set up");
			}			
		});
		
		btnLoadTickers.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event) {
				TickerDataLoad.run();
				lblLoadTickers.setText("Tickers have been loaded");
			}			
		});
		
		btnLoadOrdersWeb.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event) {
				OrderCacheFromWeb.run();
				lblLoadOrdersWeb.setText("Orders loaded from web");
			}			
		});
		
		btnLoadOrdersFile.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event) {
				OrderCacheFromFile.run();
				lblLoadOrdersFile.setText("Orders loaded from file");
			}			
		});
		
		btnSpotTick.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event) {
				if (RedisCacheOperations.readStringFromCache("SimulateSpots") != null &&
						RedisCacheOperations.readStringFromCache("SimulateSpots").equalsIgnoreCase("ON")) {
					RedisCacheOperations.writeStringToCache("SimulateSpots", "OFF");
					lblSpotTick.setText("Spot Tick Simulation stopped");
				}
				else {
					Thread simulator = new Thread(new SpotTickSimulator());
					simulator.start();
					lblSpotTick.setText("Spot Tick Simulation started");
				}				
			}			
		});
		
		btnClearCache.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event) {				
				RedisCacheOperations.ClearDB();
				lblClearCache.setText("Cache has been cleared");
			}			
		});
		
		guiFrame.setLayout(new GridLayout(0,2,10,10));		
		guiFrame.add(buttonPanel);
		guiFrame.add(labelPanel);
		guiFrame.setVisible(true);
	}
	
}
