package mktDataSimulator;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class OrderCacheFromFile {
	public static void run() {
		Path pathToFile = Paths.get(SimulatorCore.getConfig().getProperty("OrdersLoadFile"));
		String line = null;
		List <Double> scores = new ArrayList<Double>();
		List <String> values = new ArrayList<String>();

		try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) {			
			while ((line = br.readLine()) != null) {
				String[] orderInfo = line.split(",");
				if (orderInfo.length == 7 && orderInfo[0].equalsIgnoreCase("OrderID") == false) {					
					scores.add(Double.valueOf(orderInfo[0]));
					values.add(String.join("|", orderInfo));
				}
			}			
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
		}

		if (scores != null && values != null) {
			RedisCacheOperations.writeSetToCache("OrderCache", scores, values);
		}
	}
}
