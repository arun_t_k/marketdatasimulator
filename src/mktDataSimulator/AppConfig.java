package mktDataSimulator;
import java.util.Properties;

public class AppConfig
{
	Properties configFile;
	
	public AppConfig()
	{
		configFile = new java.util.Properties();
		try {
			configFile.load(this.getClass().getClassLoader().
					getResourceAsStream("config.properties"));
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public String getProperty(String key)
	{		
		return this.configFile.getProperty(key);
	}
}