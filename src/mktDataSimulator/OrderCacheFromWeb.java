package mktDataSimulator;

import java.util.List;

import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.filters.NodeClassFilter;
import org.htmlparser.nodes.TextNode;
import org.htmlparser.tags.TableRow;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

public class OrderCacheFromWeb {

	static int orderCnt = 0;

	public static void run() {

		List<String> tickerList = RedisCacheOperations.readSetFromCache("TickerList"); 
		String baseUrl = "https://finance.yahoo.com/quote/%s/options?p=%s";		

		NodeFilter trFilter = new NodeClassFilter(TableRow.class);
		NodeFilter txFilter = new NodeClassFilter(TextNode.class);

		Parser parser;
		String url = "";
		for (String ticker: tickerList) {
			try {
				url = String.format(baseUrl, ticker, ticker);
				parser = new Parser(url);				
				String fullHtml = parser.parse(null).toHtml();
				int expiryIndex = fullHtml.indexOf("{\"expirationDates\":[") + ("{\"expirationDates\":[").length();
				String[] expiries = fullHtml.substring(expiryIndex , fullHtml.indexOf("]",expiryIndex)).split(",");

				for (int k = 0; k<expiries.length; k++) {
					System.out.println (url + "&date=" + expiries[k]);
					parser = new Parser(url);
					NodeList list = parser.extractAllNodesThatMatch(trFilter);
					saveNodeData (list.extractAllNodesThatMatch(txFilter, true).toNodeArray());
				}
			}
			catch (ParserException e) {
				System.out.println (url + " returned error.");
			}
		}
	}

	private static void saveNodeData(Node[] nodes)
	{				
		for(int i = 0; i<nodes.length; i+=11) {
			String contract = nodes[i+0].getText();
			if (contract.charAt(contract.length()-9) == 'C' &&
					nodes[i+10].getText().equals("0.00%") == false) {
				String[] key = {String.valueOf(++orderCnt), //Contract Score						
						contract.substring(0, contract.length()-15),
						contract.substring(contract.length()-15, contract.length()-9),
						nodes[i+2].getText(), //Strike
						nodes[i+4].getText(), //Bid
						nodes[i+5].getText(), //Ask
						nodes[i+10].getText()}; //ImpliedVol

				RedisCacheOperations.writeSetToCache("OrderCache", orderCnt, String.join("|", key));
				//System.out.println(orderCnt + " - " + String.join("|", key));
			}
		}
	}

}
