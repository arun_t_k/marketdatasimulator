package mktDataSimulator;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TickerDataLoad {

	public static void run() {		
		Path pathToFile = Paths.get(SimulatorCore.getConfig().getProperty("TickersLoadFile"));
		String line = null;
		HashMap <String, String> tickerMap = new HashMap<>();
		List <String> tickerList = new ArrayList<String>();

		try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) {			
			while ((line = br.readLine()) != null) {
				String[] tickerInfo = line.split(",");
				if (tickerInfo.length == 4 && tickerInfo[0].equalsIgnoreCase("Symbol") == false) {					
					tickerList.add(tickerInfo[0]);
					tickerMap.put(tickerInfo[0]+"_Name",tickerInfo[1]);
					tickerMap.put(tickerInfo[0]+"_Price",tickerInfo[2]);
					tickerMap.put(tickerInfo[0]+"_TickVol",tickerInfo[3]);					
				}
			}			
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
		}

		if (tickerList != null && tickerMap != null) {
			RedisCacheOperations.writeSetToCache("TickerList", tickerList);
			RedisCacheOperations.writeHashToCache("TickerInfo", tickerMap);
		}
	}

}
