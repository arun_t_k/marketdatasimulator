package mktDataSimulator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Random;

public class GeometricBrownian {    
    private double mu;
    private double sigma;
    private double price;    
    private double deltat;
    Random psuedoRandom;
    DecimalFormat df = new DecimalFormat("#.##");    
    
    public GeometricBrownian(double mu, double sigma, double price, double T, int steps, int seed) {
        this.mu = mu;
        this.sigma = sigma;
        this.price = Math.round(price*100)/100;
        this.deltat = T / (double)steps;
        this.psuedoRandom = new Random(seed);
        df.setRoundingMode(RoundingMode.HALF_DOWN);
    }
    
    public String getSequence() {
        
    	double tmp = price + 
    			     mu * price * deltat + 
    			     sigma * price * psuedoRandom.nextGaussian() * Math.sqrt(deltat);    	
    	price = tmp;    	
        return df.format(tmp);
    }
}
