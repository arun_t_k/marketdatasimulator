package mktDataSimulator;

public class SimulatorCore {

private static RedisCacheManager cache;
private static AppConfig config;
	
	public static RedisCacheManager getCache() {
		System.out.println("Cache fetched from core!!!");
		return cache;
	}
	public static void setCache(RedisCacheManager cache) {
		SimulatorCore.cache = cache;
	}
	
	public static AppConfig getConfig() {
		System.out.println("Config fetched from core!!!");
		return config;
	}
	
	public static void main(String[] args) {		
		config = new AppConfig();
				
		new UserGuiManager();
		//cache = new RedisCacheManager();
		//TickerDataLoad.run();
		//OrderCacheFromWeb.run();
		//OrderCacheFromFile.run();
		//SpotTickSimulator.run();
	}	
}
