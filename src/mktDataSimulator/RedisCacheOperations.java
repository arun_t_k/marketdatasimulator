package mktDataSimulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

public class RedisCacheOperations {
	private static RedisCacheManager cache = SimulatorCore.getCache();

	public static String readStringFromCache(String key){
		try (Jedis jedis = cache.jedisPool.getResource()) {
			return jedis.get(key);
		}	
	}

	public static void writeStringToCache(String key, String value){
		try (Jedis jedis = cache.jedisPool.getResource()) {
			jedis.set(key, value);
		}	
	}

	public static List<String> readListFromCache( String key, int start, int end)
	{
		try (Jedis jedis = cache.jedisPool.getResource()) {
			return jedis.lrange(key, start, end);
		}		
	}

	public static List<String> readListFromCache(String key, int start) {

		return readListFromCache(key, start, -1);
	}

	public static List<String> readListFromCache(String key) {

		return readListFromCache(key, 0, -1);
	}

	public static void writeListToCache(String key, String value) {
		try (Jedis jedis = cache.jedisPool.getResource()) {			
			jedis.lpush(key, value);
		}
	}

	public static void writeListToCache(String key, List <String> values) {
		try (Jedis jedis = cache.jedisPool.getResource()) {			
			Pipeline p = jedis.pipelined();
			for (String value: values) {
				jedis.lpush(key, value);
			}
			p.sync();
		}
	}

	public static void writeHashToCache(String key, HashMap<String,String> map) {
		try(Jedis jedis = cache.jedisPool.getResource()){
			jedis.hmset(key, map);
		}
	}

	public static void writeHashToCache(String key, String subKey,String value) {
		try(Jedis jedis = cache.jedisPool.getResource()){
			jedis.hset(key, subKey, value);
		}
	}

	public static String readHashFromCache(String key, String subKey) {
		try(Jedis jedis = cache.jedisPool.getResource()){
			return jedis.hget(key, subKey);
		}
	}

	public static void writeSetToCache(String key, double score, String value) {
		try (Jedis jedis = cache.jedisPool.getResource()) {			
			jedis.zadd(key, score, value);
		}
	}

	public static void writeSetToCache(String key, String value) {
		try (Jedis jedis = cache.jedisPool.getResource()) {			
			jedis.sadd(key, value);
		}
	}

	public static void writeSetToCache(String key, List <String> values) {
		try (Jedis jedis = cache.jedisPool.getResource()) {			
			Pipeline p = jedis.pipelined();
			for (String value: values) {
				jedis.sadd(key, value);
			}
			p.sync();
		}
	}

	public static List<String> readSetFromCache(String key){
		try (Jedis jedis = cache.jedisPool.getResource()) {
			List<String> setMembers = new ArrayList<String>();
			for(String s : jedis.smembers(key)) setMembers.add(s);
			return setMembers;			
		}
	}

	public static void writeSetToCache(String key, List<Double> scores,List<String> values) {
		try (Jedis jedis = cache.jedisPool.getResource()) {
			if (scores.size() == values.size()){
				Pipeline p = jedis.pipelined();
				for (int i = 0; i<scores.size(); i++) {
					jedis.zadd(key, scores.get(i),values.get(i));
				}
				p.sync();
			}
		}
	}
	
	public static void ClearDB() {
		try (Jedis jedis = cache.jedisPool.getResource()) {			
			System.out.println("Clearing DB - " + jedis.flushDB());
		}
	}
}